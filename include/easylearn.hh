//easylearn.hh

#ifndef EASYLEARN_HH_
#define EASYLEARN_HH_

#include <exception>
#include <string>
#include <vector>
#include <cmath>
#include <memory>



namespace learn_
{



//=============================================================================
//=============================================================================
//=============================================================================
//=== EXCEPTIONS DEFINITION ===================================================
//=============================================================================
//=============================================================================
//=============================================================================

struct Exception : public std::exception
{
    Exception(std::string const& msg):
    _msg("[EasyLearn.exception : " + msg + "]")
    {
    }


    virtual ~Exception()
    {
    }


    virtual const char* what() const noexcept
    {
        return _msg.c_str();
    }

private:
    std::string const _msg;
};



//=============================================================================
//=============================================================================
//=============================================================================
//=== AGGREGATION =============================================================
//=============================================================================
//=============================================================================
//=============================================================================



class Aggregation //interface
{
public:
    virtual ~Aggregation();
    virtual double aggregate(std::vector<double> const& weights, std::vector<double> const& inputs) = 0;
    virtual std::vector<double> prime(std::vector<double> const& weights, std::vector<double> const& inputs) = 0; //return the derivative according to each weight
    virtual void learn(double gradient, double learningRate, double momentum) = 0;
};



class Dot : public Aggregation
{
public:
    double aggregate(std::vector<double> const& weights, std::vector<double> const& inputs)
    {
        double result = 0;
        for(unsigned i = 0; i < weights.size(); i++)
        {
            result += (weights[i] * inputs[i]);
        }
        return result;
    }


    std::vector<double> prime(std::vector<double> const& weights, std::vector<double> const& inputs)
    {
        weights[0]; //avoid the "unused" warning
        return inputs;
    }


    void learn(double gradient, double learningRate, double momentum)
    {
        //nothing to learn
        gradient = gradient + learningRate + momentum; //useless but avoid the "unused" warning
    }
};



class Distance : public Aggregation
{
public:
    Distance(unsigned order) : _order(order)
    {
    }


    double aggregate(std::vector<double> const& weights, std::vector<double> const& inputs)
    {
        double result;
        for(unsigned i=0; i<inputs.size(); i++)
        {
            result += static_cast<double>(std::pow((inputs[i] - weights[i]), _order));
        }
        return std::pow(result, 1/_order);
    }


    std::vector<double> prime(std::vector<double> const& weights, std::vector<double> const& inputs)
    {
        double a = std::pow(aggregate(weights, inputs), (1-_order));
        std::vector<double> result(weights.size(), 0);

        for(unsigned i = 0; i < weights.size(); i++)
        {
            result[i] += (-std::pow((inputs[i] - weights[i]), _order-1) * a);
        }
        return result;
    }


    void learn(double gradient, double learningRate, double momentum)
    {
        //nothing to learn
        gradient = gradient + learningRate + momentum; //useless but avoid the "unused" warning
    }


protected:
    unsigned const _order;
};



//=============================================================================
//=============================================================================
//=============================================================================
//=== ACTIVATION ==============================================================
//=============================================================================
//=============================================================================
//=============================================================================



class Activation //interface
{
public:
    virtual ~Activation();
    virtual std::pair<double, unsigned> activate(std::vector<double> val) = 0; //double is the result, unsigned is the index of the used val
    virtual double prime(double val) = 0;
    virtual void learn(double gradient, double learningRate, double momentum) = 0;
};


//=============================================================================
//=============================================================================
//=============================================================================
//=== NEURON ==================================================================
//=============================================================================
//=============================================================================
//=============================================================================


class Neuron
{
protected:
    unsigned _weightSetNumber; //k
    std::vector<std::vector<double>> _weights; //nested vectors are the weights of a set. The external vector is the weight set list.
    std::vector<double> _bias; //each bias is associated to a weight set.

    std::shared_ptr<Activation> _Activation; //should be a value but polymorphism is needed
    std::shared_ptr<Aggregation> _Aggregation; //should be a value but polymorphism is needed
};



}


#endif //EASYLEARN_HH_